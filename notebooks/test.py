import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import os
import json
import mne
from matplotlib.animation import FuncAnimation
from ipywidgets import interact
from openbci.preprocess import eeg_filters
from mne.decoding import CSP
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.svm import SVC

filename = "2019-07-19 17_41_51.820255"
# filename = "2019-11-18 11_00_30.869375"
# filename = '2019-11-26 14_50_47.191628'

data = np.loadtxt(f"data/{filename}.csv", delimiter=',')
print(f"Input data size: {data.shape}")

plt.figure(figsize=(20, 5), dpi=90)

# Only plot the fist column
plt.subplot(121)
col1 = data[:, 0]
plt.plot(col1[:1000])
plt.title("Channel 1")

# and the markers
plt.subplot(122)
markers = data[:, -1]  # the marker are in the last column
plt.plot(markers)
plt.title("Markers")

plt.show()

descriptor = json.load(open(f"data/{filename}.json", 'r'))
print(descriptor)

# format the descriptor
sample_rate = float(descriptor['sample_rate'].replace(' Hz', ''))
start_marker = descriptor['start_marker']
stop_marker = descriptor['end_marker']
left_marker = descriptor['left_marker']
right_marker = descriptor['right_marker']
channels_names = descriptor['channels'].split(',')
channels_names = [ch.upper().strip().capitalize() for ch in channels_names]

print(f"Sample rate: {sample_rate}")
print(f"Channels: {channels_names}")
print("-" * 70)

# Markers positions
start = (np.where(data[:, -1] == start_marker)[0])
stop = (np.where(data[:, -1] == stop_marker)[0])
right = (np.where(data[:, -1] == right_marker)[0])
left = (np.where(data[:, -1] == left_marker)[0])

print(f"Start marker position: {start}")
print(f"Right marker positions: {right}")
print("-" * 70)

# vector time from sample rate
time = np.linspace(0, data.shape[0] / sample_rate, data.shape[0]) - start / sample_rate

fig = plt.figure(figsize=(16, 9), dpi=90)
gridsize = (3, 1)


# EEG plot
ax1 = plt.subplot2grid(gridsize, (0, 0), rowspan=2)
plt.title(filename)

# replace the y ticks with the channels names
ax1.set_yticks(range(0, len(channels_names)))
ax1.set_yticklabels(channels_names)

[plt.plot(time, d[0] - np.mean(d[1]) + d[1]) for d in enumerate(data[:, :-1].T)]
plt.grid(True)


# Markers plot
ax2 = plt.subplot2grid(gridsize, (2, 0))
plt.plot(time, data[:, -1])
plt.grid(True)

# markers legends
plt.plot(time[start], data[:, -1][start], 'o', label='Start')
plt.plot(time[right], data[:, -1][right], 'o', label='Right')
plt.plot(time[left], data[:, -1][left], 'o', label='Left')
plt.plot(time[stop], data[:, -1][stop], 'o', label='Stop')
plt.legend()

plt.xlabel('Time [$s$]')
plt.show()

montage = mne.channels.make_standard_montage('standard_1020')
info = mne.create_info(channels_names, sfreq=sample_rate, ch_types="eeg", montage=montage)

# first 'column' of eeg
values = data[:, :-1][0]

plt.figure(figsize=(5, 5), dpi=90)
mne.viz.plot_topomap(values, info, names=channels_names, show_names=True)
plt.show()


def get_markers(data, marker_name, width, offset=0):

    eeg_data = data[:, :-1]
    markers = data[:, -1]

    # filtering with openbci
    eeg_data = eeg_filters.band330(eeg_data)
    eeg_data = eeg_data**2

    # data centralization
    eeg_data = eeg_data - eeg_data.mean(axis=0)

    marker = descriptor[marker_name]
    index = np.argwhere(markers == marker).reshape(-1)

    print(f"Index for {marker_name}: {index}")
    return [eeg_data[start + offset:start + width].copy() for start in index]


O = 0.4
offset = int(sample_rate * O)

W = 1
width = int(sample_rate * W)  # the first N seconds

rights = get_markers(data, 'right_marker', width, offset)
lefts = get_markers(data, 'left_marker', width, offset)

print('-' * 70)
print(f"{len(rights)} right markers")
print(f"{len(lefts)} left markers")


def historical(data, N, name=""):

    # how N topoplots for trial
    fig, axes = plt.subplots(nrows=1, ncols=N, figsize=(30, 8), dpi=90)
    for i, d in enumerate(np.linspace(0, len(data) - 1, N)):
        values = data.copy()[int(d)]
        mne.viz.plot_topomap(values, info, axes=axes[i], show=False)

    # time as title
    for i, t in enumerate(np.linspace(O, W, N)):
        axes[i].set_title(f"{t:.02}s")

    if name:
        axes[0].set_ylabel(name)

    plt.show()


historical(rights[0], N=12, name="Right")
historical(lefts[0], N=12, name="Left")


# Split input and outputs
Xraw = np.concatenate((rights, lefts))
Xraw = np.array([x.T for x in Xraw])

y = np.array([1] * len(rights) + [-1] * len(lefts))  # 1:derecha y -1:izquierda

print(f"Input: {Xraw.shape}")
print(f"Output: {y.shape}")

csp_epochs = CSP(n_components=8, reg='empirical', log=True, norm_trace=False, transform_into='average_power')
csp_epochs.fit(Xraw, y)

scaler = StandardScaler()
Xcsp = csp_epochs.transform(Xraw)
Xdataz = scaler.fit_transform(Xcsp)
pca = PCA(n_components=2)
Z = pca.fit_transform(Xdataz)

fig = plt.figure(figsize=(9, 6), dpi=90)
plt.title("Right Vs Left")

yc = ["C0" if (i == -1) else 'C1' for i in y]
plt.scatter(*Z.T, c=yc, zorder=1)

s = np.append(Z, y.reshape(y.shape[0], 1), axis=1)
Zf = Z[np.sign(s[:, 0]) != np.sign(s[:, 2])]
plt.scatter(*Zf.T, marker='o', c='w', s=200, edgecolors='red', zorder=0, label='Fails')

plt.scatter(*Z[0], c="C0", label='Right')
plt.scatter(*Z[-1], c="C1", label='Left')
plt.legend()

plt.show()
