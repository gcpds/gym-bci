import os
from push import command

py_sdist_build = 'python setup.py sdist build'
command(py_sdist_build)

last_dist = sorted(filter(lambda s: s.endswith('.tar.gz'), os.listdir('dist')))[-1]
py_twine = f'twine upload dist/{last_dist}'
command(py_twine)
