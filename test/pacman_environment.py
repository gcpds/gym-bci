import gym
import gym_bci
import random
import time

env = gym.make('bci-pacman-v0')
actions = ['north', 'south', 'east', 'west']

N = 5

# env.reset(chosenLayout='originalClassic')
# [env.step(random.choice(actions)) for i in range(N)]
# env.close()

# env.reset(chosenLayout='originalClassic', no_ghosts=True)
# [env.step(random.choice(actions)) for i in range(N)]
# env.close()


# aa = {
    # "no_ghosts": False,
    # "size": 15,
    # "nghosts": 4,
    # "npellets": 10,
# }


# env.reset(randomLayout=True, **aa)
# [env.step(random.choice(actions)) for i in range(N)]
# env.close()


actions = [('north', (11, 1)),
           ('south', (11, 20)),
           ('east', (1, 11)),
           ('west', (20, 11)),
           ]

env.reset(chosenLayout=f"train")
for action, start in actions:

    env.step('move', pacman_location=start)
    [env.step(action) for i in range(20)]

    time.sleep(0.1)
env.close()
