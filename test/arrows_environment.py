import gym
import gym_bci
import time
import logging
import random

logging.getLogger().setLevel(logging.INFO)

env = gym.make('bci-arrows-v0')


environ_config = {
    'maintain': 500,
    'min_delay': 20,
    'max_delay': 80,
    'button_start': True,
}


env.reset(**environ_config)


actions = ['right', 'left', 'south', 'north']

for i in range(5):
    env.step(random.choice(actions))

env.close()
