import os
import sys

if len(sys.argv) > 1:
    commit_msg = sys.argv[1]
else:
    version_str = open(os.path.join('gym_bci', '_version.txt'), 'r').read().strip()
    commit_msg = f"Release {version_str}"


def command(cmd): return os.system(cmd)


git_add = 'git add .'
git_commit = f'git commit -m "{commit_msg}"'
git_push = 'git push'

command(git_add)
command(git_commit)
command(git_push)
