Arrows environment
==================

Initialization
--------------

Can be created like a normal ``Gym`` envirnonment.

.. code:: ipython3

    import gym
    import gym_bci
    
    env = gym.make('bci-arrows-v0')
    env.reset()

Actions
-------

The ``arrows`` environment contains a set of actions: ``right``,
``left``, ``up``, ``down``; for compatibility reasons the next set of
actions are equivalent: ``east``,\ ``west``,\ ``north``,\ ``south``.

.. code:: ipython3

    # actions = ['right', 'left', 'up', 'down']
    actions = ['east', 'west', 'north', 'south']

The actions are performed with the ``step`` method.

.. code:: ipython3

    env.step('left')
    env.close()

|image0|

.. |image0| image:: images/arrow.png

.. code:: ipython3

    import random
    for i in range(4):
        env.step(random.choice(actions))
    env.close()

Configuration
-------------

The configuration for each environment are passed in the ``reset``
method.

.. code:: ipython3

    environment_config = {
        'maintain': 4000,  # the time for display the arrow
        'min_delay': 200,  # the min time for delay after show the arrow
        'max_delay': 800,  # the max time for delay after show the arrow
        'button_start': True,  # show a wait button that holds the interpreter until is pressed 
    }
    
    env.reset(**environment_config)
    env.step('up')
    env.close()


.. parsed-literal::

    /home/yeison/Development/gcpds/gym-bci/notebooks/gym_bci/envs/arrows_env.py:79: MatplotlibDeprecationWarning: Adding an axes using the same arguments as a previous axes currently reuses the earlier instance.  In a future version, a new instance will always be created and returned.  Meanwhile, this warning can be suppressed, and the future behavior ensured, by passing a unique label to each axes instance.
      pyplot.subplot(111)

