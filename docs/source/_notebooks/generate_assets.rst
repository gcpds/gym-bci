.. code:: ipython3

    from matplotlib import pyplot
    
    import gym
    import gym_bci
    
    env = gym.make('bci-arrows-v0')
    env.reset()


.. parsed-literal::

    /home/yeison/Development/gcpds/gym-bci/notebooks/gym_bci/envs/pacman


.. code:: ipython3

    import numpy as np
    
    N = 1
    a = ['left']*N + ['right']*N + ['up']*N + ['down']*N
    
    np.random.shuffle(a)
    
    
    for i, a_ in enumerate(a):
        env.savefig(a_, f"images/a{str(i*2).rjust(2, '0')}.png")
    
    env.close()



.. parsed-literal::

    Chose layout smallClassic




.. parsed-literal::

    array([[[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            ...,
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]],
    
           [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            ...,
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]],
    
           [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            ...,
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]],
    
           ...,
    
           [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            ...,
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]],
    
           [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            ...,
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]],
    
           [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            ...,
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]]], dtype=uint8)



.. code:: ipython3

    import random
    import time
    import os
    import gym
    import gym_bci
    
    actions = ['west', 'east', 'north', 'south']
    
    env = gym.make('bci-pacman-v0')
    env.reset(chosenLayout='smallClassic')
    time.sleep(5)
    
    for i in range(15):
        env.step(random.choice(actions))
    #     time.sleep(1)
        os.system(f"spectacle -a -n -b -o images/pacman-{str(i*2).rjust(2, '0')}.png")
    #     time.sleep(1)
        print(i)
        
    env.close()


.. parsed-literal::

    /home/yeison/Development/gcpds/gym-bci/notebooks/gym_bci/envs/pacman
    Chose layout smallClassic
    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14

