Pacman enviroment
=================

Initialization
--------------

Can be created like a normal ``Gym`` envirnonment.

.. code:: ipython3

    import gym
    import gym_bci
    
    env = gym.make('bci-pacman-v0')


.. parsed-literal::

    /home/yeison/Development/gcpds/gym-bci/notebooks/gym_bci/envs/pacman


|image0|

.. |image0| image:: images/pacman_classic.png

Actions
-------

.. code:: ipython3

    import random 
    actions = ['north', 'south', 'east', 'west', 'stop']
    
    env.reset()
    [env.step(random.choice(actions)) for i in range(10)]
    env.close()


.. parsed-literal::

    Chose layout capsuleClassic


Layouts
-------

.. code:: ipython3

    import time
    
    for layout in env.layouts:
        env.reset(chosenLayout=layout)
        time.sleep(1)
        
    env.close()


.. parsed-literal::

    Chose layout capsuleClassic
    Chose layout contestClassic
    Chose layout mediumClassic
    Chose layout mediumGrid
    Chose layout minimaxClassic
    Chose layout openClassic
    Chose layout originalClassic
    Chose layout smallClassic
    Chose layout smallGrid
    Chose layout testClassic
    Chose layout trappedClassic
    Chose layout trickyClassic


Configuration
-------------

The configuration for each environment are passed in the ``reset``
method.

For randomly generated
~~~~~~~~~~~~~~~~~~~~~~

Is possible to change que number of ghosts, then size of the map and the
number of pellets.

.. code:: ipython3

    environment_config = {
        "size": 15,
        "nghosts": 4,
        "npellets": 10,
    }
    
    env.reset(randomLayout=True, **environment_config)
    [env.step(random.choice(actions)) for i in range(20)]
    env.close()

For choosen layout
~~~~~~~~~~~~~~~~~~

It only is possible to deactivate the ghosts.

.. code:: ipython3

    environment_config = {
        "no_ghosts": True, 
    }
    
    env.reset(chosenLayout="trickyClassic", **environment_config)
    [env.step(random.choice(actions)) for i in range(20)]
    env.close()


.. parsed-literal::

    Chose layout trickyClassic_noGhosts


Training layouts
----------------

There is an special action: ``move``, that can be used for teleport
Pacman, is useful in the training environment.

.. code:: ipython3

    import time
    
    actions = [('north', (11, 1)),
               ('south', (11, 20)),
               ('east', (1, 11)),
               ('west', (20, 11)),
               ]
    
    env.reset(chosenLayout="train")
    time.sleep(0.2)
    
    for action, start in actions:
        env.step('move', pacman_location=start)
        [env.step(action) for i in range(20)]
        time.sleep(0.1)
        
    env.close()


.. parsed-literal::

    Chose layout train

