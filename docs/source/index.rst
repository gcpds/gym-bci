.. Gym-BCI documentation master file, created by
   sphinx-quickstart on Sat Nov 23 14:14:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Gym-BCI
=======

A `GYM <https://gym.openai.com/>`_ environment for `BCI` applications.
This module was designed for work along with `OpenBCI <https://openbci.readthedocs.io/en/latest/>`_
The main code with sample data can be found in the `repository <https://bitbucket.org/gcpds/gym-bci>`_.

.. image:: _notebooks/images/actions.gif
   :align: center

.. image:: _notebooks/images/pacman.gif
   :align: center

.. warning::
     * This module is still under development, is unstable and not recommended for use in production.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   _notebooks/arrows_env
   _notebooks/pacman_env
   _notebooks/example_openbci
   _notebooks/mi_classification


Contact:
--------
Main developer: Yeison Cardona yencardonaal@unal.edu.co, `@yeisondev <https://twitter.com/yeisondev>`_


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
